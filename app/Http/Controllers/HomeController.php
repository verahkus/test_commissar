<?php

namespace App\Http\Controllers;

use App\Model\Classifiers\City;
use App\Model\Classifiers\Country;
use App\Model\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home', [
//            'clients' => null,
        ]);
    }

    /**
     * @author verahkusmd
     * данные для jstree
     * @return GET \Illuminate\Http\JsonResponse
     */
    public function jstreeData()
    {
        $countries = Country::orderByDesc('name')->get();
        $cities = City::orderBy('name')->get();
        $clients = Client::get();
        $res_data = collect();

        foreach ($countries as $country) {
            $res_data->push([
                'text' => $country->name,
                'id' => 'country_'.$country->id,
                'country_id' => $country->id,
                'click_country' => true,
                'parent' => '#'
            ]);
        }

        foreach ($cities as $city) {
            $res_data->push([
                'text' => $city->name,
                'id' => 'city_'.$city->id,
                'city_id' => $city->id,
                'click_city' => true,
                'parent' => 'country_'.$city->country_id
            ]);
        }

        foreach ($clients as $client) {
            $res_data->push([
                'text' => $client->name,
                'id' => $client->id,
                'client_id' => $client->id,
                'parent' => 'city_'.$client->city_id,
                'icon' => asset('img/icon_jstree/client.png'),
                'click_client' => true,
            ]);
        }

        return response()->json($res_data);
    }

    /**
     * @author verahkusmd
     * данные по координатам клиентов для jstree клик по городу
     * @param Request $request
     * @return POST \Illuminate\Http\JsonResponse
     */
    public function getLastGeoClients(Request $request) {
        $res_data = collect();
//      клик по стране
        if ($request->click_country) {
            $country = Country::find((int)$request->country_id);
            $countryCitiesIdArray = $country->cities->pluck('id')->toArray();
            $cities = City::find($countryCitiesIdArray);
            $cityClientsIdArray = $cities->pluck('id')->toArray();
            $clients = Client::whereIn('city_id',$cityClientsIdArray)->get();

            foreach ($clients as $client) {
                self::formatDataGeoClient($res_data,$client);
            }
        }
        // клик по городу
        if ($request->click_city) {
            $city = City::find((int)$request->city_id);
            //массив id клиентов
            $cityClientsIdArray = $city->clients->pluck('id')->toArray();
            $clients = Client::find($cityClientsIdArray);

            foreach ($clients as $client) {
                self::formatDataGeoClient($res_data,$client);
            }
        }
        // клик по клиенту
        if ($request->click_client) {
            $client = Client::find((int)$request->client_id);
            self::formatDataGeoClient($res_data,$client);
        }

        return response()->json(array(
            'result'=>true,
            'data'=>$res_data,
            'click_city'=>$request->click_city,
            'click_client'=>$request->click_client,
            'click_country'=>$request->click_country,
        ));
    }

    /**
     * @author verahkusmd
     * форматирование коллекции клиента(ов) для вывода на карту
     * @param $res_data - коллекция на вывод
     * @param $client - модель клиента
     * @return bool
     */
    public function formatDataGeoClient($res_data, $client) {
        $tmp_geos = $client->geos->first();
        if ($tmp_geos) {
            $latitude = $tmp_geos->latitude;
            $longitude = $tmp_geos->longitude;
            $res_data->push([
                'id' => $client->id,
                'name' => $client->name,
                'email' => $client->email,
                'phone' => $client->phone,
                'lat' => $latitude,
                'lng' => $longitude,
                'count_geos' => $tmp_geos = $client->geos->count(),
            ]);
            return $res_data;
        }
        return false;
    }

    /**
     * @author verahkusmd
     * получение координат маршрута за сегодня
     * @param Request $request
     * @return POST \Illuminate\Http\JsonResponse
     */
    public function getLastGeoRoute(Request $request) {
        $res_data = collect();
        $client = Client::find((int)$request->client_id);
        return response()->json(array(
            'result'=>true,
            'data'=>$client->geos_day,
        ));
    }
}
