<?php

namespace App\Model;

use App\Model\Classifiers\City;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function geos()
    {
        return $this->hasMany(ClientGeo::class)->orderBy('created_at');
    }
    public function geos_day()
    {
        return $this->hasMany(ClientGeo::class)
            ->where('created_at', '>=', Carbon::today())
            ->orderByDesc('created_at');
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
