<?php

namespace App\Model\Classifiers;

use App\Model\ClientGeo;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
