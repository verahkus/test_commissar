<?php

namespace App\Model\Classifiers;

use App\Model\Client;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function clients()
    {
        return $this->hasMany(Client::class);
    }
}
