**Тестовое задание для вакансии https://moikrug.ru/vacancies/1000045035**

```
база данных sqlite
database\database.sqlite

.env
Стартовые координаты карты (Россия)
latitude=61.6987
longitude=99.5054
DB_DATABASE='полный путь'/database/database.sqlite

npm run dev
php artisan migrate --seed

в тестовых данных: 
пользователь: admin@test.com пароль 12345678
Российская Федерация - Москва - Петров Иван Иванович (есть маршрут)
+ у одного из клинетов из Санкт-Петербурга (id 114)

```
