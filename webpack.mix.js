let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
mix.styles([
        'public/css/app.css',
        // 'resources/assets/css/jstree.min.css'
    ], 'public/css/app.css').version();

mix.scripts([
    'public/js/app.js',
    // 'resources/assets/js/jstree.js'
], 'public/js/app.js').version();