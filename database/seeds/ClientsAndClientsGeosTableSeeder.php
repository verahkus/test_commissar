<?php

use Illuminate\Database\Seeder;

class ClientsAndClientsGeosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 100; $i++) {
            DB::table('clients')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'phone' => $faker->tollFreePhoneNumber,
                'city_id' => $faker->numberBetween($min = 0, $max = 200),
            ]);
        }
        DB::table('clients')->insert([
            'id' => 0,
            'name' => 'Петров Иван Иванович',
            'email' => 'test@test.ru',
            'phone' => '79150747654',
            'city_id' => 0,
        ]);
        for ($i = 0; $i < 10; $i++) {
            DB::table('clients')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'phone' => $faker->tollFreePhoneNumber,
                'city_id' => 0,
            ]);
        }
        for ($i = 0; $i < 10; $i++) {
            DB::table('clients')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'phone' => $faker->tollFreePhoneNumber,
                'city_id' => 501,
            ]);
        }
        DB::table('client_geos')->insert([
            'client_id' => 0,
            'latitude' => '55.6324',
            'longitude' => '37.6198',
            'created_at' => \Carbon\Carbon::now()->subSeconds(10),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 0,
            'latitude' => '55.6407',
            'longitude' => '37.6199',
            'created_at' => \Carbon\Carbon::now()->subSeconds(12),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 0,
            'latitude' => '55.6520',
            'longitude' => '37.6198',
            'created_at' => \Carbon\Carbon::now()->subSeconds(13),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 0,
            'latitude' => '55.6628',
            'longitude' => '37.6200',
            'created_at' => \Carbon\Carbon::now()->subSeconds(14),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 0,
            'latitude' => '55.6646',
            'longitude' => '37.6410',
            'created_at' => \Carbon\Carbon::now()->subSeconds(15),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 101,
            'latitude' => '55.5660',
            'longitude' => '37.6595',
            'created_at' => \Carbon\Carbon::now()->subSeconds(16),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 102,
            'latitude' => '55.7415',
            'longitude' => '37.3766',
            'created_at' => \Carbon\Carbon::now()->subSeconds(18),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 103,
            'latitude' => '55.8992',
            'longitude' => '37.5936',
            'created_at' => \Carbon\Carbon::now()->subSeconds(19),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 104,
            'latitude' => '55.7601',
            'longitude' => '37.8545',
            'created_at' => \Carbon\Carbon::now()->subSeconds(22),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 112,
            'latitude' => '59.9876',
            'longitude' => '30.3962',
            'created_at' => \Carbon\Carbon::now()->subSeconds(23),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 113,
            'latitude' => '59.9077',
            'longitude' => '30.5171',
            'created_at' => \Carbon\Carbon::now()->subSeconds(24),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 114,
            'latitude' => '59.9256',
            'longitude' => '30.3193',
            'created_at' => \Carbon\Carbon::now()->subSeconds(25),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 114,
            'latitude' => '59.8863',
            'longitude' => '30.3220',
            'created_at' => \Carbon\Carbon::now()->subSeconds(25),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 114,
            'latitude' => '59.8400',
            'longitude' => '30.3234',
            'created_at' => \Carbon\Carbon::now()->subSeconds(25),
        ]);
        DB::table('client_geos')->insert([
            'client_id' => 114,
            'latitude' => '59.8497',
            'longitude' => '30.1847',
            'created_at' => \Carbon\Carbon::now()->subSeconds(25),
        ]);
    }
}
