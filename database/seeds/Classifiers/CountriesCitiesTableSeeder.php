<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            DB::table('countries')->insert([
                'name' => $faker->country,
            ]);
        }
        DB::table('countries')->insert([
            'id' => 0,
            'name' => 'Российская Федерация',
        ]);

        for ($i = 0; $i < 500; $i++) {
            DB::table('cities')->insert([
                'name' => $faker->city,
                'country_id' => $faker->numberBetween($min = 0, $max = 49),
                'latitude' => $faker->latitude($min = 0, $max = 90),
                'longitude' => $faker->longitude($min = 0, $max = 180),
            ]);
        }
        DB::table('cities')->insert([
            'id' => 0,
            'name' => 'Москва',
            'country_id' => 0,
            'latitude' => 55.7532,
            'longitude' => 37.6225,
        ]);
        // id - 501
        DB::table('cities')->insert([
            'name' => 'Санкт-Петербург',
            'country_id' => 0,
            'latitude' => 55.7532,
            'longitude' => 37.6225,
        ]);

    }
}
