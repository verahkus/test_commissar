@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
@endsection
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            {{--<div class="card">--}}
                {{--<div class="card-header text-center">@lang('site.title_map_clients')</div>--}}

                {{--<div class="card-body">--}}

                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header text-center">@lang('site.title_clients_list')</div>
                                <div class="card-body jstee">
                                    <div id="jstree">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="card">
                                <div class="card-header text-center">@lang('site.title_map')</div>
                                <div class="card-body">
                                    <div id="map"></div>
                                    <div id="control_panel" class="mt-2">
                                        <p id="directions_panel"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3ynJXvxIULks2zX1aKUAZLsqcAJC7yKw"></script>

@endsection

@push('javascript')
    <script>
        $(document).ready(function(){

            // var tree = $('#jstree').jstree();
            var tree = $('#jstree').jstree({
                'core' : {
                    'data' : {
                        'url' : '{{action('HomeController@jstreeData')}}',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });

            // клик по элементу дерева
            tree.bind("select_node.jstree", function (e,data) {
                // console.log(data.node.original);
                //отслеживаем клик по стране или городу или клиенту
                if (data.node.original.click_country || data.node.original.click_city || data.node.original.click_client) {
                    $.ajax({
                        url: '{{action('HomeController@getLastGeoClients')}}',
                        method: 'POST',
                        data: {
                            click_city: data.node.original.click_city,
                            click_client: data.node.original.click_client,
                            click_country: data.node.original.click_country,
                            city_id:data.node.original.city_id,
                            country_id:data.node.original.country_id,
                            client_id:data.node.original.client_id
                        },
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(res)
                        {
                            // console.log(res);
                            if (res.result) {
                                clearOverlays();
                                if (res.data.length!=0) {
                                    // ставим маркеры
                                    initMarkers(res.data);
                                    if (res.click_client=='true') {
                                        map.setZoom(10);
                                    }
                                }
                                else {
                                    map.setCenter(new google.maps.LatLng({{config('app.latitude')}}, {{config('app.longitude')}}));
                                    if (res.click_country=='true') {
                                        alert('Клиентов в стране нет.');
                                    }
                                    if (res.click_client=='true') {
                                        alert('Данных о клиенте (координат) нет.');
                                    }
                                    if (res.click_city=='true') {
                                        alert('Клиентов в городе нет.');
                                    }
                                    map.setZoom(3);
                                }
                            }
                        },
                        error: function(msg)
                        {
                            alert('Ошибка. Обратитесь к администратору. 0001')
                            console.log(msg);
                        }
                    });
                }

            });

            function clickSubmit() {
                //клик по "подробнее" клиента
                $(".submit_client").click(function() {
                    var client_id = $(this).attr('data-id');
                    $.ajax({
                        url: '{{action('HomeController@getLastGeoRoute')}}',
                        method: 'POST',
                        data: {
                            client_id:client_id
                        },
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(res)
                        {
                            // console.log(res);
                            if (res.result) {
                                var arrLastGeo = res.data;
                                if (arrLastGeo.length>2) {
                                    var first = arrLastGeo.shift();
                                    var last = arrLastGeo.pop();
                                    origin = new google.maps.LatLng(first.latitude, first.longitude);
                                    destination = new google.maps.LatLng(last.latitude, last.longitude);

                                    waypts = [];
                                    $.each(res.data, function( key, value ) {
                                        waypts.push({
                                            location: new google.maps.LatLng(value.latitude, value.longitude),
                                            stopover: true
                                        });
                                    });
                                    calcRoute(origin,destination,waypts);
                                } else {
                                    alert('Данных для построения маршрута нет или не достаточно.')
                                }

                            }
                        },
                        error: function(msg)
                        {
                            alert('Ошибка. Обратитесь к администратору. 0002');
                            console.log(msg);
                        }
                    });

                });
            }

            var map, infoWindow;
            var markersArray = [];
            var directionsArray = [];
            function initMap() {
                var centerLatLng = new google.maps.LatLng({{config('app.latitude')}}, {{config('app.longitude')}});

                var mapOptions = {
                    center: centerLatLng,
                    zoom: 3
                };
                map = new google.maps.Map(document.getElementById("map"), mapOptions);
                infoWindow = new google.maps.InfoWindow();
                google.maps.event.addListener(map, "click", function() {
                    infoWindow.close();
                });

                directionsDisplay = new google.maps.DirectionsRenderer();
                directionsDisplay.setMap(map);
            }

            google.maps.event.addDomListener(window, "load", initMap);

            function initMarkers(markersData) {
                var bounds = new google.maps.LatLngBounds();
                for (var i = 0; i < markersData.length; i++){
                    var latLng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
                    var id = markersData[i].id;
                    var name = markersData[i].name;
                    var address = markersData[i].address;
                    var email = markersData[i].email;
                    var phone = markersData[i].phone;
                    var count_geos = markersData[i].count_geos;
                    // добавим маркер
                    addMarker(latLng, id, name, email, phone, count_geos);
                    bounds.extend(latLng);
                }
                map.fitBounds(bounds);
            }

            function addMarker(latLng, id, name, email, phone, count_geos) {
                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    title: name
                });
                markersArray.push(marker);
                google.maps.event.addListener(marker, "click", function() {
                    var contentString = '<div class="infowindow text-center">' +
                        '<h4>' + name + '</h4>' +
                        '<p>' + email + '</p>' +
                        '<p>' + phone + '</p>' +
                        '<p> точек: ' + count_geos + '</p>' +
                        '<button class="btn btn-primary btn-sm mb-2 submit_client" data-id="'+id+'">Маршрут</button>' +
                        '</div>';
                    infoWindow.setContent(contentString);
                    infoWindow.open(map, marker);
                    clickSubmit();
                });
            }

            function clearOverlays() {
                for (var i = 0; i < markersArray.length; i++ ) {
                    markersArray[i].setMap(null);
                }
                directionsDisplay.setDirections({routes: []});

            }

            var directionDisplay;
            var directionsService = new google.maps.DirectionsService();

            function calcRoute(origin,destination,waypoints) {
                var request = {
                    origin: origin,
                    destination: destination,
                    waypoints: waypoints,
                    optimizeWaypoints: true,
                    travelMode: google.maps.DirectionsTravelMode.WALKING
                };
                directionsService.route(request, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                        directionsArray.push(directionsDisplay);
                        var route = response.routes[0];
                        var summaryPanel = document.getElementById("directions_panel");
                        summaryPanel.innerHTML = "";
                        // For each route, display summary information.
                        for (var i = 0; i < route.legs.length; i++) {
                            var routeSegment = i + 1;
                            summaryPanel.innerHTML += "<b>Точка: " + routeSegment + "</b><br />";
                            summaryPanel.innerHTML += route.legs[i].start_address + " to ";
                            summaryPanel.innerHTML += route.legs[i].end_address + "<br />";
                            summaryPanel.innerHTML += route.legs[i].distance.text + "<br /><br />";
                        }
                    } else {
                        alert("directions response " + status);
                    }
                });
            }

        });
    </script>
@endpush
